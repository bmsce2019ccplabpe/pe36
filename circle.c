#include<stdio.h>
#include<math.h>
#define pi 3.14
float get_radius()
{
	float radius;
	printf("enter radius\n");
	scanf("%f", &radius);
	return radius;
}

float compute_area(float radius)
{
	float area;
	area = pi * radius * radius;
	return area;
}

float compute_circumference(float radius)
{
	float circumference;
	circumference = 2 * pi * radius;
	return circumference;
}

void output_area(float radius, float area)
{
	printf("the area of the circle with radius=%f is %f\n", radius, area);
}

void output_circumference(float radius, float circumference)
{
	printf("the circumference of the circle with radius=%f is %f\n", radius,
	       circumference);
}

int main()
{
	float radius, area, circumference;
	radius = get_radius();
	area = compute_area(radius);
	circumference = compute_circumference(radius);
	output_area(radius, area);
	output_circumference(radius, circumference);
	return 0;
}